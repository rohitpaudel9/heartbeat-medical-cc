import re
from collections import Counter

def readFile(filename):
    with open(filename) as filestream:
        log = filestream.read()
        return log

def calculateAverage(list):        
    avg = sum(list) / len(list)
    return avg 

def totalQueries(log):
    regex_query = r'operationType: query'
    
    queries = re.findall(regex_query, log)
    print('Total Number of Queries performed is: ', len(queries))

def totalMutations(log):
    regex_mutation = r'operationType: mutation'
    mutations = re.findall(regex_mutation, log)
    print('Total Numbers of Queries performed is: ', len(mutations))

def totalSubscriptions(log):
    regex_subscription = r'operationType: subscription' 
    subscriptions = re.findall(regex_subscription, log)
    print('Total Numbers of Subscriptions performed is: ', len(subscriptions))

def operationExtractor(log):
    regex_operation_extractor = r'\[graphql\] operation \| operation: (.*?) \| opId: '
    operations = re.findall(regex_operation_extractor, log)
    operation_counts = Counter(operations)
    for x,y in operation_counts.items():
        print('The counts for {} is {}.'.format(x,y))

def durationGroupedByOperationType(log, opt_type):
    regex = r'\[graphql\] operation-responsetime \| operation: \w[a-zA-Z]* \| duration: (.*?) \| operationType\: (.\w[a-zA-Z]*)'
    all_duration = re.findall(regex, log)
    #print(all_duration)
    query = []
    for i in all_duration:
        if(i[1] == opt_type):
            query.append(float(i[0]))
    return query
    


def durationGroupedByOperation(log):
    regex = r'\[graphql\] operation-responsetime \| operation: (.*?) \| duration: (.*?) \| operationType\: \w[a-zA-Z]*'
    all_duration = re.findall(regex, log)
    print(all_duration)

    
def printQuestionOne(log):
    print('')
    print('Question 1. How many queries, mutations and subscriptions have been performed?')
    print('')
    print('Ans: The number of queries, mutations and subscriptions performed is as following: ')
    print('')
    totalQueries(log)
    totalMutations(log)
    totalSubscriptions(log)
    print('')

def printQuestionTwo(log):
    print('Question 2. What are the counts for the different operations?')
    print('')
    print('Ans: The counts for different operations performed is as following: ')
    print('')
    operationExtractor(log)
    print('')

def printQuestionThree(log):
    query = durationGroupedByOperationType(log, 'query')
    subscription = durationGroupedByOperationType(log, 'subscription')
    mutation = durationGroupedByOperationType(log, 'mutation')
    print('Question 3. What are the average duration times grouped by a. operation type and b. operation?')
    print('')
    print('Ans a): The average duration time group by operation type is as following: ' )
    print('')
    print('By Operation Type Query: {} '.format(calculateAverage(query)))
    print('By Operation Type Subscription: {} '.format(calculateAverage(subscription)))
    print('By Operation Type Mutation: {} '.format(calculateAverage(mutation)))
    print('')
    print('Ans b): The average duration time group by operation is as following: ')
    print('')
    print('By Operation ... : ')
    print('By Operation ... : ')
    print('By Operation ... : ')
    print('')
    
def printQuestionFour(log):
    query = durationGroupedByOperationType(log, 'query')
    subscription = durationGroupedByOperationType(log, 'subscription')
    mutation = durationGroupedByOperationType(log, 'mutation')
    print('Question 4. What are the max duration times grouped by a. operation type and b. operation?')
    print('')
    print('Ans a): The max duration time group by operation type is as following: ' )
    print('')
    print('By Operation Type Query: {} '.format(max(query)))
    print('By Operation Type Query: {} '.format(max(subscription)))
    print('By Operation Type Query: {} '.format(max(mutation)))
    print('')
    print('Ans b): The max duration time group by operation is as following: ')
    print('')
    print('By Operation ... : ')
    print('By Operation ... : ')
    print('By Operation ... : ')
    print('')

def printQuestionFive(log):
    query = durationGroupedByOperationType(log, 'query')
    subscription = durationGroupedByOperationType(log, 'subscription')
    mutation = durationGroupedByOperationType(log, 'mutation')
    print('Question 5. What are the min duration times grouped by a. operation type and b. operation?')
    print('')
    print('Ans a): The min duration time group by operation type is as following: ' )
    print('')
    print('By Operation Type Query: {} '.format(min(query)))
    print('By Operation Type Query: {} '.format(min(subscription)))
    print('By Operation Type Query: {} '.format(min(mutation)))
    print('')
    print('Ans b): The min duration time group by operation is as following: ')
    print('')
    print('By Operation ... : ')
    print('By Operation ... : ')
    print('By Operation ... : ')
    print('')

if __name__ == "__main__":
    log = readFile('logs.log')
    printQuestionOne(log)
    printQuestionTwo(log)
    printQuestionThree(log)   
    printQuestionFour(log) 
    printQuestionFive(log)



