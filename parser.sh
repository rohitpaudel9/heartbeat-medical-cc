#!/bin/bash 

echo "Welcome to Heartbeat Medical Log Parser Program by Rohit P."
echo

printf "Total number of Queries performed is: "  
grep "operationType: query" logs.log -c 
echo
printf "Total number of Mutations performed is: "
grep "operationType: mutation" logs.log -c
echo
printf "Total number of Subscriptions performed is: "
grep "operationType: subscription" logs.log -c
echo
grep "[graphql] operation | operation: (.+?) | *"
