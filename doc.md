## choosing tools
    - awk: https://pubs.opengroup.org/onlinepubs/9699919799/utilities/awk.html
    - grep with bash
    - python script with regex
    - nodejs stream : https://www.npmjs.com/package/event-stream

## Tool with final implementation and why?
    In the end, I implemented the solution with python script and some regex. In part 2, I have to save the result in database. It is relatively easy to use sqlite database with python. I find it bit complicated to integrate database with bash script although the code is clean, short yet powerful.

## Running the code
- virtual env: 
    to activate: ´source parser/bin/activate´
    to deactivate: ´deactivate´

- `python 3 parser.py`